# README #

These scripts are meant to be used with TamperMonkey for Google Chrome or GreaseMonkey for Firefox

### What is this repository for? ###

This repository contains all of the individual tweak files used by members of Compass FSS Data Services.

### How do I get set up? ###

* Install [Tampermonkey from Chrome Web Store]
* Install [FSS Remedy Tasks XL] for an expanded task list
* Install [FSS Task Extender] for a significantly larger and more useful details window in tasks

### Who do I talk to? ###

* You may be able to reach out to ryan.beaman on Slack for further assistance

[Tampermonkey from Chrome Web Store]: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en
[FSS Remedy Tasks XL]: https://bitbucket.org/compass_dataservices/data_services_remedy_enhancements/raw/HEAD/FSS_Remedy_Tasks_XL.user.js
[FSS Task Extender]: https://bitbucket.org/compass_dataservices/data_services_remedy_enhancements/raw/HEAD/FSS_Task_Extender.user.js