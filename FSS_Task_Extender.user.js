// ==UserScript==
// @name         FSS Task Extender
// @namespace    http://bitbucket.org/compass_dataservices
// @version      2.3.5
// @updateURL    https://bitbucket.org/compass_dataservices/data_services_remedy_enhancements/raw/HEAD/FSS_Task_Extender.user.js
// @downloadURL  https://bitbucket.org/compass_dataservices/data_services_remedy_enhancements/raw/HEAD/FSS_Task_Extender.user.js
// @description  Enhancements to the way that data services requests are displayed in RemedyWeb
// @author       WisdomWolf
// @match        remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3ATask/Support/*
// @match        remedyweb.compass-usa.com/arsys/forms/remedyprod.NA.CompassGroup.Corp/FSS%3ATask/Support/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @require      https://code.jquery.com/jquery-2.2.4.min.js
// @grant        GM_setClipboard
// ==/UserScript==
/* jshint -W097 */
'use strict';
const IG_TYPE = "InfoGenesis";
const SIMP_TYPE = "Simphony";
const AD_HOC = "DS Change Ad Hoc";
const REPORT_REQ = "DS Report Request";
const COMPLETED = "Completed";
const WIP = "Work In Progress";
const ON_HOLD = "On Hold";
const RYAN = "Ryan Beaman";
const JOHN = "John Jones";
const MARTA = "Marta Wetzell";
const AMANDA = "Amanda Beaman";
const CAROLINA = "Carolina Avila";

var unitNum = $('#arid_WIN_0_750550094');
var unitName = $('#arid_WIN_0_1000005061');
var ticketNum = $('#arid_WIN_0_1');
var assignedInd = $('#arid_WIN_0_240000015');
var taskID = $('#arid_WIN_0_1');
var assignedGroup = $('#arid_WIN_0_240000006');
var fssSolution = $('#arid_WIN_0_536870928');
var typeSelection = $('#arid_WIN_0_200000004');
var itemSelection = $('#arid_WIN_0_200000005');
var statusSelection = $('#arid_WIN_0_750550045');
var wipState = $('#arid_WIN_0_750550026');
var workLog = $('#arid_WIN_0_240000008');
var summaryText = $('#arid_WIN_0_8');
var createEmailBtn = $('#WIN_0_750550007').children()[0];
var glCodeInput = $('#arid_WIN_0_536870951');
var saveSearchBtn = $('#TBsearchsavechanges');
var numKiosksDiv = $('#WIN_0_536870953');
var numKiosks = $('#arid_WIN_0_536870953');
var numTerminalsDiv = $('#WIN_0_777702124');
var numTerminals = $('#arid_WIN_0_777702124');
var numP2PEDiv = $('#WIN_0_536870957');
var numP2PE = $('#arid_WIN_0_536870957');
var orderPayKiosks = $('#WIN_0_777901017');
var selfCheckOutKiosks = $('#WIN_0_777901018');
var numTablets = $('#WIN_0_777901019');
var numIntegratedP2PE = $('#WIN_0_777901020');
var numSCODiv = $('#WIN_0_777752060');

var deviceQtyFields = [
	numKiosks, numTerminals, numP2PE, orderPayKiosks, 
	selfCheckOutKiosks, numTablets, numIntegratedP2PE,
	numSCODiv
]

var textFields = [
	assignedInd, typeSelection, itemSelection, 
	statusSelection, wipState, workLog, summaryText, 
	taskID, unitNum, unitName
]

$('.text').addClass('mousetrap');
textFields = textFields.concat(deviceQtyFields);
textFields.forEach(function (element, index) {
	element.addClass('mousetrap');
});


function renameTab() {

	if (unitNum.val() == "99999") {
		document.title = ticketNum.val();
	} else if (unitNum.val() != "" || unitName.val() != "") {
		document.title = unitNum.val() + " - " + unitName.val();
	} else {
		document.title = "New/Search";
	}

}

function rearrangeWindow() {
    if (document.title == "New/Search") {
        if (saveSearchBtn.text() == "Search") {
            console.log('binding enter key for faster searching');
            Mousetrap.bind('enter', goSaveOrSearch);
        };
        return;
    }
	if (assignedGroup.val() == "FSS Data Services Team" || fssSolution.val() == "Data Services") {
		var sibTasksTable = $("#WIN_0_350002000");
		var sibTasksLabel = $("#WIN_0_536870929");
		var detContainer = $("#WIN_0_536870937");
		var detailsText = $("#arid_WIN_0_536870937");
		var detButton = detContainer.find(".btn.btn3d.expand");
		var bottomFrame = $('#WIN_0_350301000');

		var fieldsToHide = [
			sibTasksTable, sibTasksLabel, numKiosksDiv, 
			numTerminalsDiv, numP2PEDiv, orderPayKiosks, 
			selfCheckOutKiosks, numTablets, numIntegratedP2PE,
			numSCODiv
		]

		sibTasksTable.css({
			"top" : "356px",
			"left" : "13px",
			"width" : "258px",
			"height" : "63px"
		});
		//sibTasksTable.style.top = "356px";
		//sibTasksTable.style.left = "13px";
		//sibTasksTable.style.width = "258px";
		//sibTasksTable.style.height = "63px";

		detContainer.css({
			"left" : "345px",
			"top" : "100px",
			"height" : "284px",
			"width" : "649px"
		});
		//detContainer.style.left = "345px";
		//detContainer.style.top = "100px";
		//detContainer.style.height = "284px";
		//detContainer.style.width = "649px";

		detailsText.css({
			"width" : "626px",
			"height" : "268px"
		});
		detailsText.attr("readonly", true);
		detailsText.addClass("mousetrap");
		//detailsText.style.width = "626px";
		//detailsText.style.height = "268px";

		detButton.css("left", "630px");
		//detButton.style.left = "630px";

		fieldsToHide.forEach(function(element, index) {
			element.hide();
		});

		// sibTasksTable.hide();
		// sibTasksLabel.hide();
		// numKiosksDiv.hide();
		// numTerminalsDiv.hide();
		// numP2PEDiv.hide();
		// orderPayKiosks.hide();
		// selfCheckOutKiosks.hide();
		// numTablets.hide();
		// numIntegratedP2PE.hide();

		setTimeout(function () {
			detailsText.click();
			detailsText.focus();
		}, 1000);
	} else {
		updateField(glCodeInput, "645260");
        setDeviceQty();
		wipState.bind('change', wipStateHandler);
        console.log('Tisk Task');
	}
}

function wipStateHandler() {
    if (wipState.val() == "Installation") {
        var posTask = getPOSTask();
        updateField(workLog, "Billed under " + posTask);
    }
}

function getPOSTask() {
    var $POSTask;
	$('#T350002000 tr').each(function (row) {
		var $Type = $(this).find('td').eq(7).find('nobr > span').text();
		if ($Type == 'Point of Sale') {
			$POSTask = $(this).find('td').eq(0).find('nobr > span').text();
			console.log('POS Task: ' + $POSTask);
		}
	});
    //updateField(workLog, $POSTask);
    return $POSTask;
}

function setDeviceQty() {
    deviceQtyFields.forEach(function (element, index) {
	if (element.val() == '') {
        updateField(element, 0);
    } else {
        console.log('element text set to ' + element.text);
    }
});

}

function updateField(element, value) {
	element.val(value);
	triggerChange(element);
}

function triggerChange(element) {
	if (element.length != undefined) {
		element = element[0];
	}
	if ("createEvent" in document) {
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		element.dispatchEvent(evt);
	} else {
		element.fireEvent("onchange");
	}
}

function markComplete() {
	if (!workLog.val()) {
		updateField(workLog, COMPLETED);
	}
	statusSelection.click();
	//updateField(statusSelection, COMPLETED);
}

function goSaveOrSearch() {
    saveSearchBtn.click();
	return false;
}

setTimeout(renameTab, 500);
setTimeout(rearrangeWindow, 1000);

Mousetrap.bind('ctrl+,', function (e) {
	alert('Hello, I am Lindsay Lohan! -Achmed');
});

Mousetrap.bind('ctrl+s', goSaveOrSearch);

Mousetrap.bind('ctrl+shift+f', function (e) {
	markComplete();
	return false;
});

Mousetrap.bind('ctrl+i', function (e) {
	updateField(typeSelection, IG_TYPE);
	updateField(itemSelection, AD_HOC);
	return false;
});

Mousetrap.bind('ctrl+space i a', function (e) {
	updateField(typeSelection, IG_TYPE);
	updateField(itemSelection, AD_HOC);
	return false;
});

Mousetrap.bind('ctrl+space i r', function (e) {
	updateField(typeSelection, IG_TYPE);
	updateField(itemSelection, REPORT_REQ);
	return false;
});

Mousetrap.bind('ctrl+space s a', function (e) {
	updateField(typeSelection, SIMP_TYPE);
	updateField(itemSelection, AD_HOC);
	return false;
});

Mousetrap.bind('ctrl+space s r', function (e) {
	updateField(typeSelection, SIMP_TYPE);
	updateField(itemSelection, REPORT_REQ);
	return false;
});

Mousetrap.bind('ctrl+space t r', function (e) {
	updateField(assignedInd, RYAN);
	return false;
});

Mousetrap.bind('ctrl+space t j', function (e) {
	updateField(assignedInd, JOHN);
	return false;
});

Mousetrap.bind('ctrl+space t a', function (e) {
	updateField(assignedInd, AMANDA);
	return false;
});

Mousetrap.bind('ctrl+space t c', function (e) {
	updateField(assignedInd, CAROLINA);
	return false;
});

Mousetrap.bind('ctrl+space t m', function (e) {
	updateField(assignedInd, MARTA);
	return false;
});

Mousetrap.bind('ctrl+e', function (e) {
	createEmailBtn.click();
});
